/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "deviceinfo_swupdate.h"
#include "version_files.h"
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "dinfo-swu"

static deviceinfo_swupdate_app_t app;
static amxb_bus_ctx_t* deviceinfo_ctx = NULL;
static amxp_timer_t* timer_retry_start = NULL;

static void continue_initialization(UNUSED amxp_timer_t* timer, UNUSED void* data) {
    SAH_TRACEZ_IN(ME);
    int ret_value = -1;
    const char* deviceinfo_path = amxc_var_constcast(cstring_t, amxo_parser_get_config(app.parser, "deviceinfo"));
    const char* hwrevision_path = amxc_var_constcast(cstring_t, amxo_parser_get_config(app.parser, "hwrevision"));

    deviceinfo_ctx = amxb_be_who_has(deviceinfo_path);
    //Sometimes it hangs (5% of the times when booting up), even with a signal from amxb. This is a workaround and must be removed in the future.
    if((deviceinfo_ctx == NULL) && (timer_retry_start == NULL)) {
        SAH_TRACEZ_ERROR(ME, "No DeviceInfo bus context found - retrying again in 3 seconds.");
        ret_value = amxp_timer_new(&timer_retry_start, continue_initialization, NULL);
        when_failed_trace(ret_value, exit, ERROR, "Failed to initialize the timer, the plugin will stop.");
        ret_value = amxp_timer_start(timer_retry_start, 3000);
        when_failed_trace(ret_value, exit, ERROR, "Failed to start the timer, the plugin will stop.");
        goto rearm;
    }
    when_null_trace(deviceinfo_ctx, exit, ERROR, "No DeviceInfo bus context found. The plugin will stop.");

    version_files_create(deviceinfo_path, hwrevision_path);
exit:
    amxp_timer_delete(&timer_retry_start);
rearm:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void wait_for_deviceinfo_cb(UNUSED const char* signame,
                                   UNUSED const amxc_var_t* const data,
                                   UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    continue_initialization(NULL, NULL);
    SAH_TRACEZ_OUT(ME);
    return;
}

static void deviceinfo_swupdate_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int ret_value = -1;
    const char* deviceinfo_path = NULL;
    const char* hwrevision_path = NULL;
    amxc_string_t exp;

    amxc_string_init(&exp, 0);

    app.dm = dm;
    app.parser = parser;

    deviceinfo_path = amxc_var_constcast(cstring_t, amxo_parser_get_config(app.parser, "deviceinfo"));
    hwrevision_path = amxc_var_constcast(cstring_t, amxo_parser_get_config(app.parser, "hwrevision"));

    deviceinfo_ctx = amxb_be_who_has(deviceinfo_path);
    if(deviceinfo_ctx == NULL) {
        amxb_wait_for_object(deviceinfo_path);
        amxc_string_setf(&exp, "^wait:%s$", deviceinfo_path);
        amxc_string_replace(&exp, ".", "\\\\.", UINT32_MAX);
        ret_value = amxp_slot_connect_filtered(NULL, amxc_string_get(&exp, 0), NULL, wait_for_deviceinfo_cb, NULL);
        when_failed_trace(ret_value, exit, ERROR, "Failed to wait for DeviceInfo to be available");
        SAH_TRACEZ_WARNING(ME, "Waiting for DeviceInfo");
        goto exit;
    }

    version_files_create(deviceinfo_path, hwrevision_path);
exit:
    amxc_string_clean(&exp);
    SAH_TRACEZ_OUT(ME);
}

static void deviceinfo_swupdate_clean(void) {
    amxp_timer_delete(&timer_retry_start);
    version_files_clean();
    app.dm = NULL;
    app.parser = NULL;
}

amxd_dm_t* get_dm(void) {
    return app.dm;
}

amxo_parser_t* get_parser(void) {
    return app.parser;
}

amxc_var_t* get_config(void) {
    return &(app.parser->config);
}

int _deviceinfo_swupdate_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = 0;

    SAH_TRACEZ_INFO(ME, "deviceinfo_swupdate_main, reason: %i", reason);
    switch(reason) {
    case AMXO_START:
        deviceinfo_swupdate_init(dm, parser);
        SAH_TRACEZ_INFO(ME, "swupdate extension for deviceinfo-manager started");
        break;
    case AMXO_STOP:
        deviceinfo_swupdate_clean();
        SAH_TRACEZ_INFO(ME, "swupdate extension for deviceinfo-manager stopped");
        break;
    default:
        retval = -1;
        break;
    }

    return retval;
}
