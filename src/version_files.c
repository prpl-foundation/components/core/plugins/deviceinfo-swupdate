/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>
#include <unistd.h>
#include <libgen.h>

#include "deviceinfo_swupdate.h"
#include "version_files.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#define ME "dinfo-swu"

static bool subscribed = false;

static amxb_bus_ctx_t* deviceinfo_ctx = NULL;
static amxc_string_t* deviceinfo_path = NULL;
static amxc_string_t* hwrevision_path = NULL;

static int get_base_path(const cstring_t swversions_path_str, amxc_string_t* base_path) {
    int status = -1;
    cstring_t dir = NULL;
    cstring_t dir_name = NULL;

    amxc_string_reset(base_path);
    when_str_empty(swversions_path_str, exit);

    amxc_string_setf(base_path, "%s", swversions_path_str);
    if((strcmp(amxc_string_get(base_path, 0), "/") != 0) && (amxc_string_search(base_path, "/", 0) != -1)) {
        dir = strdup(swversions_path_str);
        dir_name = dirname(dir);
        amxc_string_setf(base_path, "%s/", dir_name);
        free(dir);
    }

    status = 0;
exit:
    return status;
}

static int generate_hw_revision_file(const cstring_t modelname, const cstring_t hwrevision) {
    int rv = -1;
    int status = -1;
    FILE* file = NULL;
    amxc_string_t* tmp_file_path = NULL;
    const cstring_t hwrevision_path_str = NULL;

    amxc_string_new(&tmp_file_path, 0);

    hwrevision_path_str = amxc_string_get(hwrevision_path, 0);
    if(str_empty(hwrevision_path_str)) {
        hwrevision_path_str = DEFAULT_PATH_HW_REVISION;
    }

    get_base_path(hwrevision_path_str, tmp_file_path);
    amxc_string_appendf(tmp_file_path, "%s", TMP_PATH_HW_REVISION);

    //Delete current hwrevision file
    if(access(amxc_string_get(tmp_file_path, 0), F_OK) == 0) {
        remove(amxc_string_get(tmp_file_path, 0));
    }

    file = fopen(amxc_string_get(tmp_file_path, 0), "w");
    when_null_trace(file, exit, ERROR, "Failed to create file %s", amxc_string_get(tmp_file_path, 0));
    if(str_empty(modelname)) {
        fprintf(file, "%s ", DEFAULT_MODELNAME);
    } else {
        fprintf(file, "%s ", modelname);
    }
    if(str_empty(hwrevision)) {
        fprintf(file, "%s", DEFAULT_HWREVISION);
    }
    fclose(file);

    //Delete current hwrevision file, if it exists
    if(access(hwrevision_path_str, F_OK) == 0) {
        remove(hwrevision_path_str);
    }

    //Rename the tmp file to "create" the hwrevision file atomically.
    when_false_trace(access(amxc_string_get(tmp_file_path, 0), F_OK) == 0, exit, ERROR, "File %s could not be created.", amxc_string_get(tmp_file_path, 0));
    rv = rename(amxc_string_get(tmp_file_path, 0), hwrevision_path_str);
    when_failed_trace(rv, exit, ERROR, "Failed to rename file and create the %s", hwrevision_path_str);
    when_false_trace(access(hwrevision_path_str, F_OK) == 0, exit, ERROR, "File %s could not be created.", hwrevision_path_str);
    remove(amxc_string_get(tmp_file_path, 0));

    status = 0;
exit:
    amxc_string_delete(&tmp_file_path);
    return status;
}

int version_files_manual_create(void) {
    int rv = 0;
    int status = 0;
    const cstring_t modelname = NULL;
    const cstring_t hwrevision = NULL;
    const amxc_var_t* deviceinfo_var = NULL;
    amxc_var_t bus_var;

    amxc_var_init(&bus_var);

    if(deviceinfo_ctx != NULL) {
        amxb_get(deviceinfo_ctx, amxc_string_get(deviceinfo_path, 0), 0, &bus_var, 1);
        deviceinfo_var = GETP_ARG(&bus_var, "0.0.");
        if(deviceinfo_var != NULL) {
            modelname = GET_CHAR(deviceinfo_var, "ModelName");
        }
    }

    rv = generate_hw_revision_file(modelname, hwrevision);
    if(rv != 0) {
        SAH_TRACEZ_ERROR(ME, "Failed to generate the hwrevision file");
        status = -1;
    }

    amxc_var_clean(&bus_var);
    return status;
}

/**
   @brief
   Callback function that changes the version files.

   @param[in] sig_name Signal/event name.
   @param[in] data Variant with the event data.
   @param[in] priv Not used.
 */
static void create_files_cb(const char* const sig_name,
                            const amxc_var_t* const data,
                            UNUSED void* const priv) {
    const cstring_t path = GET_CHAR(data, "path");
    (void) sig_name;// when tracing is disabled, variable is not used anymore

    SAH_TRACEZ_NOTICE(ME, "Received event: %s", sig_name);

    when_str_empty_trace(path, exit, ERROR, "Path does not exist.");
    version_files_manual_create();
exit:
    return;
}

static void initialize_variables(void) {
    if(deviceinfo_path == NULL) {
        amxc_string_new(&deviceinfo_path, 0);
    }
    if(hwrevision_path == NULL) {
        amxc_string_new(&hwrevision_path, 0);
    }
}

void version_files_clean(void) {
    amxb_unsubscribe(amxb_be_who_has(amxc_string_get(deviceinfo_path, 0)),
                     amxc_string_get(deviceinfo_path, 0),
                     create_files_cb,
                     NULL);
    amxc_string_delete(&deviceinfo_path);
    amxc_string_delete(&hwrevision_path);
    deviceinfo_path = NULL;
    hwrevision_path = NULL;
}

static int subscribe_to_deviceinfo_parameters(const cstring_t p_deviceinfo_path) {
    int rv = -1;
    amxc_string_t expr;

    amxc_string_init(&expr, 0);

    when_str_empty_trace(p_deviceinfo_path, exit, ERROR, "The first time the function CreateSWVersionFile is called the argument DeviceInfoPath must be non-empty string.");

    amxc_string_setf(deviceinfo_path, "%s", p_deviceinfo_path);
    amxc_string_setf(&expr, "notification in ['dm:object-changed'] && path matches '%s$' && (contains('parameters.ModelName') || contains('parameters.SoftwareVersion'))", amxc_string_get(deviceinfo_path, 0));

    deviceinfo_ctx = amxb_be_who_has(amxc_string_get(deviceinfo_path, 0));
    when_null_trace(deviceinfo_ctx, exit, ERROR, "No DeviceInfo bus context found.");

    rv = amxb_subscribe(deviceinfo_ctx,
                        amxc_string_get(deviceinfo_path, 0),
                        amxc_string_get(&expr, 0),
                        create_files_cb,
                        NULL);
    when_failed_trace(rv, exit, ERROR, "Failed to subscribe the version file creation to the DeviceInfo parameters.");

exit:
    amxc_string_clean(&expr);
    return rv;
}

int version_files_create(const cstring_t p_deviceinfo_path,
                         const cstring_t p_hwrevision_path) {
    int status = -1;

    initialize_variables();

    if(p_hwrevision_path != NULL) {
        amxc_string_setf(hwrevision_path, "%s", p_hwrevision_path);
    }

    if(!subscribed) {
        int rv = subscribe_to_deviceinfo_parameters(p_deviceinfo_path);
        if(rv != 0) {
            version_files_clean();
            goto exit;
        }
        subscribed = true;
    }

    status = version_files_manual_create();

exit:
    return status;
}
