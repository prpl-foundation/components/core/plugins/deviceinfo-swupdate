# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Release v0.0.1 - 2023-09-13(15:42:36 +0000)

### Other

- DeviceInfo is blocking for a long time on boot

