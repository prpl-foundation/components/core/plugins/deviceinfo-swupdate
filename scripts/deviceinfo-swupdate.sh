#!/bin/sh

source /usr/lib/amx/scripts/amx_init_functions.sh

name="deviceinfo-swupdate"

case $1 in
    boot)
        process_boot ${name} -D
        ;;
    start)
        process_start ${name} -D
        ;;
    stop)
        process_stop ${name}
        ;;
    shutdown)
        process_shutdown ${name}
        ;;
    restart)
        $0 stop
        $0 start
        ;;
    debuginfo)
        ;;
    *)
        echo "Usage : $0 [start|boot|stop|shutdown|restart]"
        ;;
esac
