/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <string.h>
#include <regex.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <event2/event.h>
#include <pthread.h>
#include <math.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxc/amxc_macros.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <amxb/amxb_register.h>
#include <amxb/amxb_connect.h>
#include <amxo/amxo.h>

#include "test_common.h"

static struct event_base* base = NULL;
static int timeout = 0;

void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

void dump_object(amxd_object_t* object, int index) {
    amxd_param_t* param = NULL;
    amxd_object_t* tmp_obj = NULL;
    amxc_var_t* tmp_var = NULL;
    cstring_t obj_name = NULL;
    cstring_t param_name = NULL;
    cstring_t param_value = NULL;
    index++;

    if(object == NULL) {
        printf("The object is NULL.\n");
    } else {
        amxc_llist_for_each(it, (&object->parameters)) {
            param = amxc_llist_it_get_data(it, amxd_param_t, it);
            param_name = (cstring_t) amxd_param_get_name(param);
            tmp_var = (amxc_var_t*) amxd_object_get_param_value(object, param_name);
            param_value = amxc_var_dyncast(cstring_t, tmp_var);
            printf("param_name[%d]: %s = %s\n", index, param_name, param_value);
            free(param_value);
            param = NULL;
        }
        amxc_llist_for_each(it, (&object->objects)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            printf("obj_name[%d]: %s\n", index, obj_name);
            dump_object(tmp_obj, index);
            tmp_obj = NULL;
        }
        amxc_llist_for_each(it, (&object->instances)) {
            tmp_obj = amxc_llist_it_get_data(it, amxd_object_t, it);
            obj_name = (cstring_t) amxd_object_get_name(tmp_obj, AMXD_OBJECT_INDEXED);
            printf("instance_name[%d]: %s\n", index, obj_name);
            dump_object(tmp_obj, index);
            tmp_obj = NULL;
        }
    }
    fflush(stdout);
}

/**
   @brief
   Write string to a file.

   @param[in] fname file name.
   @param[in] string string containing the information to be written.
 */
void write_to_file(const char* fname, amxc_string_t* string) {
    FILE* p_file;

    if(access(fname, F_OK) == 0) {
        remove(fname);
    }

    p_file = fopen(fname, "w");
    fputs(string->buffer, p_file);
    fclose(p_file);
}

/**
   @brief
   Copy file.

   @param[in] fsrc Source file.
   @param[in] fdst Destination file.
 */
int copy_file(const char* fsrc, const char* fdst) {
    int c;
    FILE* stream_R;
    FILE* stream_W;

    assert_non_null(fsrc);
    assert_non_null(fdst);
    assert_string_not_equal(fsrc, "");
    assert_string_not_equal(fdst, "");
    assert_int_equal(access(fsrc, F_OK), 0);

    stream_R = fopen(fsrc, "r");
    if(stream_R == NULL) {
        return -1;
    }
    stream_W = fopen(fdst, "w");//create and write to file
    if(stream_W == NULL) {
        fclose(stream_R);
        return -2;
    }
    while((c = fgetc(stream_R)) != EOF) {
        fputc(c, stream_W);
    }
    fclose(stream_R);
    fclose(stream_W);

    assert_int_equal(access(fdst, F_OK), 0);
    return 0;
}

/**
   @brief
   Read the information from the file and save to a string.

   @param[in] fname file name.
   @param[out] string string containing the information of the file appended.
 */
void read_from_file(const char* fname, amxc_string_t* string) {
    FILE* p_file = NULL;
    cstring_t buffer = NULL;
    long length = 0;
    size_t bytes_read = 0;

    when_str_empty(fname, exit);
    when_null(string, exit);

    p_file = fopen(fname, "rb");
    when_null(p_file, exit);

    fseek(p_file, 0, SEEK_END);
    length = ftell(p_file);
    fseek(p_file, 0, SEEK_SET);
    buffer = calloc(1, length + 1);
    when_null(buffer, exit);
    bytes_read = fread(buffer, 1, length, p_file);

    if(bytes_read <= 0) {
        free(buffer);
        goto exit;
    }
    amxc_string_push_buffer(string, buffer, length + 1);

exit:
    if(p_file != NULL) {
        fclose(p_file);
    }
}

static void el_signal_timers(UNUSED evutil_socket_t fd,
                             UNUSED short event,
                             void* arg) {
    pthread_t* timer_thread = (pthread_t*) arg;
    int counter = 0;
    if(timer_thread != NULL) {
        pthread_cancel(*timer_thread);
        pthread_join(*timer_thread, NULL);
    }
    event_base_loopbreak(base);
    //Wait for the timers that are triggered sequentially to be identified
    sleep(3);
    while(counter < 2) {
        amxp_timers_calculate();
        amxp_timers_check();
        handle_events();
        sleep(1);
        counter++;
    }
}

void wait_for_sigalarm(float seconds) {
    struct event* signal_alarm = NULL;
    uint64_t timeout_ms = (uint64_t) round((seconds + 1) * 1000);
    int thread_ret;
    pthread_t timer_thread;
    timeout = 0;

    handle_events();

    base = event_base_new();
    assert_non_null(base);

    signal_alarm = evsignal_new(base,
                                SIGALRM,
                                el_signal_timers,
                                (void*) (&timer_thread));
    event_add(signal_alarm, NULL);

    thread_ret = pthread_create(&timer_thread, NULL, thread_timer, (void*) (&timeout_ms));
    assert_int_equal(thread_ret, 0);
    event_base_dispatch(base);

    pthread_cancel(timer_thread);
    pthread_join(timer_thread, NULL);
    event_del(signal_alarm);
    event_base_free(base);
    base = NULL;
    free(signal_alarm);
    assert_int_equal(timeout, 0);
    handle_events();
}

void* thread_timer(void* ptr) {
    struct timeval tval_start, tval_end, tval_elapsed;
    uint64_t* timeout_ms = (uint64_t*) ptr;
    uint64_t elapsed_time = 0;

    gettimeofday(&tval_start, NULL);

    while(elapsed_time <= (*timeout_ms) * 1000) {
        usleep(1000);
        gettimeofday(&tval_end, NULL);
        timersub(&tval_end, &tval_start, &tval_elapsed);
        elapsed_time = (uint64_t) (tval_elapsed.tv_sec * 1000000) + (uint64_t) tval_elapsed.tv_usec;
    }

    event_base_loopbreak(base);
    print_message("Test time out reached! Elapsed time: %lu, Timeout: %lu. (%s:%d)\n", elapsed_time, (*timeout_ms) * 1000, __func__, __LINE__);
    timeout = 1;
    return NULL;
}

bool assert_file_content_equals(const char* file_path, const char* expected_content) {
    bool ret = false;
    amxc_string_t* file_content = NULL;

    amxc_string_new(&file_content, 0);

    if(file_path == NULL) {
        printf("ERROR: File path is NULL\n"); fflush(stdout);
        goto exit;
    }

    if(access(file_path, F_OK) != 0) {
        printf("ERROR: File %s doesn't exist\n", file_path); fflush(stdout);
        goto exit;
    }

    read_from_file(file_path, file_content);
    if(strcmp(amxc_string_get(file_content, 0), expected_content) != 0) {
        printf("ERROR: '%s' != '%s'\n", amxc_string_get(file_content, 0), expected_content); fflush(stdout);
        goto exit;
    }

    ret = true;
exit:
    amxc_string_delete(&file_content);
    return ret;
}

void set_deviceinfo_parameters(amxd_dm_t* const dm, const char* p_swnumber, const char* p_modelname) {
    amxd_trans_t trans;
    amxc_var_t* modelname = NULL;
    amxc_var_t* swversion = NULL;

    if((p_swnumber != NULL) || (p_modelname != NULL)) {
        assert_int_equal(amxd_trans_init(&trans), 0);
        amxd_trans_select_pathf(&trans, "DeviceInfo.");
        amxc_var_new(&modelname);
        amxc_var_new(&swversion);
        if(p_modelname != NULL) {
            amxc_var_set(cstring_t, modelname, p_modelname);
            amxd_trans_set_param(&trans, "ModelName", modelname);
        }
        if(p_swnumber != NULL) {
            amxc_var_set(cstring_t, swversion, p_swnumber);
            amxd_trans_set_param(&trans, "SoftwareVersion", swversion);
        }

        assert_int_equal(amxd_trans_apply(&trans, dm), 0);
        handle_events();
        amxd_trans_clean(&trans);
        amxc_var_delete(&modelname);
        amxc_var_delete(&swversion);
    }
}
